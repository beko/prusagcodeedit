## Prusa GCode Edit Scripts

## Description
Scripts that allow you to add custom g-code at specific locations:
- [x] Before each Layer 
- [x] After each layer
- [x] At the beginning of the print
- [x] At the end of the print
- [ ] At specific layer height (in mm)

By the time of writing, it supports G-Code generated from PrusaSlicer 2.7.1

## Note
For printing wax:
1. need to reduce extruder minimum temperature (M302)
2. need to greatly increase the bed adhesion to reduce warping
    - [x] Brim -> The part disconnects with the brim after certain degree of warping
    - [x] Raft -> Does not work; raft cannot be removed from the part itself, and the bottom layer of the part has very bad quality
    - [ ] Hair spray
    - [ ] Cloth Tape
3. seems need to lift the nozzle much higher after finishing the last layer of print
4. may need to use a different style of filling method e.g. rectanlienar

For printing LDPE:
1. need to apply tape at the final heat bed temperature to maximum prevent air bubbles
   - scrap and slowly lying down the tape

## Usage
Work in progress

## Support
Work in progress

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
For open source projects, say how it is licensed.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
